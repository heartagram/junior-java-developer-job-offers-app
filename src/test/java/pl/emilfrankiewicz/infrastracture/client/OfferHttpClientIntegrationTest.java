package pl.emilfrankiewicz.infrastracture.client;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.http.Fault;
import org.apache.hc.core5.http.HttpStatus;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.util.SocketUtils;
import pl.emilfrankiewicz.infrastracture.offer.client.RemoteOfferClient;
import pl.emilfrankiewicz.infrastracture.offer.configuration.OfferHttpClientTestConfig;

import static org.assertj.core.api.BDDAssertions.then;

import java.util.Arrays;
import java.util.Collections;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;


class OfferHttpClientIntegrationTest {

    WireMockServer wireMockServer;
    int port = SocketUtils.findAvailableTcpPort();
    RemoteOfferClient remoteOfferClient = new OfferHttpClientTestConfig().remoteOfferTestClient("http://localhost", port);

    StubbedJobOffers stubbedJobOffers = new StubbedJobOffers();

    @BeforeEach
    void setup() {
        wireMockServer = new WireMockServer(options().port(port));
        wireMockServer.start();
        WireMock.configureFor(port);
    }

    @AfterEach
    void tearDown() {
        wireMockServer.stop();
    }

    @Test
    void should_fail_with_empty_response() {
        WireMock.stubFor(WireMock.get("/offers")
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.SC_OK)
                        .withHeader("Content-Type", "application/json")
                        .withFault(Fault.EMPTY_RESPONSE)));

        then(remoteOfferClient.getOffers().size()).isEqualTo(0);
    }

    @Test
    void should_return_zero_job_offers_when_status_is_no_content() {
        WireMock.stubFor(WireMock.get("/offers")
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.SC_NO_CONTENT)
                        .withHeader("Content-Type", "application/json")
                        .withBody(stubbedJobOffers.bodyWithZeroOffersJson())));

        then(remoteOfferClient.getOffers().size()).isEqualTo(0);
    }

    @Test
    void should_return_zero_job_offers_when_response_delay_is_3000_milis() {
        WireMock.stubFor(WireMock.get("/offers")
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.SC_OK)
                        .withHeader("Content-Type", "application/json")
                        .withBody(stubbedJobOffers.bodyWithZeroOffersJson())
                        .withFixedDelay(3000)));

        then(remoteOfferClient.getOffers().size()).isEqualTo(0);
    }

    @Test
    void should_return_one_job_offer() {
        WireMock.stubFor(WireMock.get("/offers")
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.SC_OK)
                        .withHeader("Content-Type", "application/json")
                        .withBody(stubbedJobOffers.bodyWithOneOfferJson())));

        then(remoteOfferClient.getOffers())
                .containsExactlyInAnyOrderElementsOf(Collections.singletonList(stubbedJobOffers.cybersourceSoftwareEngineer()));
    }

    @Test
    void should_return_two_job_offers() {
        WireMock.stubFor(WireMock.get("/offers")
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.SC_OK)
                        .withHeader("Content-Type", "application/json")
                        .withBody(stubbedJobOffers.bodyWithTwoOffersJson())));

        then(remoteOfferClient.getOffers())
                .containsExactlyInAnyOrderElementsOf(Arrays.asList(stubbedJobOffers.cybersourceSoftwareEngineer(), stubbedJobOffers.cdqJuniorDevOpsEngineer()));
    }

}