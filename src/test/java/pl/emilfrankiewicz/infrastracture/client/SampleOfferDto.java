package pl.emilfrankiewicz.infrastracture.client;

import pl.emilfrankiewicz.infrastracture.offer.configuration.dto.JobOfferDto;

interface SampleOfferDto {

    default JobOfferDto emptyOffer() {
        return new JobOfferDto();
    }

    default JobOfferDto offerWithParameters(String company, String salary, String url, String position) {
        return JobOfferDto.builder()
                .salary(salary)
                .company(company)
                .offerUrl(url)
                .title(position)
                .build();

    }
}