package pl.emilfrankiewicz.infrastracture.client;

import pl.emilfrankiewicz.infrastracture.offer.configuration.dto.JobOfferDto;

public class StubbedJobOffers {

    public String bodyWithOneOfferJson() {
        return "[{\n" +
                "    \"title\": \"Software Engineer - Mobile (m/f/d)\",\n" +
                "    \"company\": \"Cybersource\",\n" +
                "    \"salary\": \"4k - 8k PLN\",\n" +
                "    \"offerUrl\": \"https://nofluffjobs.com/pl/job/software-engineer-mobile-m-f-d-cybersource-poznan-entavdpn\"\n" +
                "  }]";
    }

    public String bodyWithTwoOffersJson() {
        return "[{\n" +
                "    \"title\": \"Software Engineer - Mobile (m/f/d)\",\n" +
                "    \"company\": \"Cybersource\",\n" +
                "    \"salary\": \"4k - 8k PLN\",\n" +
                "    \"offerUrl\": \"https://nofluffjobs.com/pl/job/software-engineer-mobile-m-f-d-cybersource-poznan-entavdpn\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"title\": \"Junior DevOps Engineer\",\n" +
                "    \"company\": \"CDQ Poland\",\n" +
                "    \"salary\": \"8k - 14k PLN\",\n" +
                "    \"offerUrl\": \"https://nofluffjobs.com/pl/job/junior-devops-engineer-cdq-poland-wroclaw-gnymtxqd\"\n" +
                "  }]";
    }

    private JobOfferDto offerWithParameters(String company, String salary, String url, String position) {
        return JobOfferDto.builder()
                .salary(salary)
                .company(company)
                .offerUrl(url)
                .title(position)
                .build();
    }

    public JobOfferDto cybersourceSoftwareEngineer() {
        return offerWithParameters("Cybersource", "4k - 8k PLN", "https://nofluffjobs.com/pl/job/software-engineer-mobile-m-f-d-cybersource-poznan-entavdpn", "Software Engineer - Mobile (m/f/d)");
    }

    public String bodyWithZeroOffersJson() {
        return "[]";
    }

    public JobOfferDto cdqJuniorDevOpsEngineer() {
        return offerWithParameters("CDQ Poland", "8k - 14k PLN", "https://nofluffjobs.com/pl/job/junior-devops-engineer-cdq-poland-wroclaw-gnymtxqd", "Junior DevOps Engineer");
    }
}
