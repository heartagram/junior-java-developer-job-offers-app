package pl.emilfrankiewicz.infrastracture.client;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import pl.emilfrankiewicz.infrastracture.offer.client.OfferClient;
import pl.emilfrankiewicz.infrastracture.offer.client.RemoteOfferClient;
import pl.emilfrankiewicz.infrastracture.offer.configuration.dto.JobOfferDto;
import java.util.List;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.when;

public class OfferClientUnitTest implements SampleOfferResponse, SampleRestTemplateExchangeResponse, SampleOfferDto {

    final RestTemplate restTemplate = Mockito.mock(RestTemplate.class);
    String uri = "http://ec2-18-157-77-69.eu-central-1.compute.amazonaws.com";
    int port = 5057;
    RemoteOfferClient offerClient = new OfferClient(restTemplate, uri, port);

    @Test
    public void should_return_one_element_list_of_offers() {
        // given
        ResponseEntity<List<JobOfferDto>> responseEntity = responseWithOneOffer();
        when(getExchange(restTemplate))
                .thenReturn(responseEntity);

        // when
        final List<JobOfferDto> offers = offerClient.getOffers();

        // then
        assertThat(offers.size()).isEqualTo(1);
    }

    @Test
    public void should_return_two_offers() {
        // given
        ResponseEntity<List<JobOfferDto>> responseEntity = responseWithOffers(emptyOffer(), emptyOffer());
        when(getExchange(restTemplate))
                .thenReturn(responseEntity);

        // when
        final List<JobOfferDto> offers = offerClient.getOffers();

        // then
        assertThat(offers.size()).isEqualTo(2);
    }

    @Test
    public void should_return_empty_list_of_offers() {
        // given
        ResponseEntity<List<JobOfferDto>> responseEntity = responseWithNoOffers();
        when(getExchange(restTemplate))
                .thenReturn(responseEntity);

        // when
        final List<JobOfferDto> offers = offerClient.getOffers();

        // then
        assertThat(offers.size()).isEqualTo(0);
    }

}
