package pl.emilfrankiewicz.infrastracture.client;

import org.mockito.ArgumentMatchers;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import pl.emilfrankiewicz.infrastracture.offer.configuration.dto.JobOfferDto;

import java.util.List;

public interface SampleRestTemplateExchangeResponse {
    default ResponseEntity<List<JobOfferDto>> getExchange(RestTemplate restTemplate) {
        return restTemplate.exchange(
                ArgumentMatchers.anyString(),
                ArgumentMatchers.any(HttpMethod.class),
                ArgumentMatchers.any(),
                ArgumentMatchers.<ParameterizedTypeReference<List<JobOfferDto>>>any());
    }
}
