package pl.emilfrankiewicz.offer;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import pl.emilfrankiewicz.offer.domain.configuration.MockMvcConfig;
import pl.emilfrankiewicz.offer.domain.dto.OfferDto;
import pl.emilfrankiewicz.offer.domain.dto.SampleOfferDto;
import pl.emilfrankiewicz.offer.domain.exceptions.OfferErrorResponse;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@WebMvcTest
@ContextConfiguration(classes = MockMvcConfig.class)
public class OfferControllerTest implements SampleOfferDto {
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    MockMvc mockMvc;

    @Test
    public void should_return_status_ok_when_get_for_offers() throws Exception {
        final List<OfferDto> expectedOfferDtos = Arrays.asList(cybersourceOfferDto(), cdqPolandOfferDto());
        String expectedResponseBody = objectMapper.writeValueAsString(expectedOfferDtos);

        final MvcResult mvcResult = mockMvc.perform(get("/offers"))
                .andExpect(status().isOk())
                .andReturn();
        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(expectedResponseBody);
    }

    @Test
    public void should_return_status_ok_when_get_for_offer_with_id() throws Exception {
        String expectedResponseBody = objectMapper.writeValueAsString(cdqPolandOffer());

        final MvcResult mvcResult = mockMvc.perform(get("/offers/24ee32b6-6b15-11eb-9439-0242ac130002"))
                .andExpect(status().isOk())
                .andReturn();
        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(expectedResponseBody);
    }

    @Test
    public void should_return_status_not_found_when_get_for_offer_with_not_found_id() throws Exception {
        OfferErrorResponse offerErrorResponse = new OfferErrorResponse("Not found offer with id: " + 666, HttpStatus.NOT_FOUND);
        String expectedResponseBody = objectMapper.writeValueAsString(offerErrorResponse);

        final MvcResult mvcResult = mockMvc.perform(get("/offers/666"))
                .andExpect(status().isNotFound())
                .andReturn();
        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(expectedResponseBody);
    }

    @Test
    public void should_return_status_ok_and_expected_offer_resource() throws Exception {
        final String cdqPolandOfferDto = objectMapper.writeValueAsString(cdqPolandOfferDto());

        final MvcResult mvcResult = mockMvc.perform(
                        post("/offers")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(cdqPolandOfferDto))
                .andExpect(status().isOk())
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(cdqPolandOfferDto);
    }


    @Test
    public void should_return_status_bad_request_and_empty_response() throws Exception {
        final MvcResult mvcResult = mockMvc.perform(post("/offers")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(wrongOfferPostJson()))
                .andExpect(status().isBadRequest())
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).contains(expectedErrorMessages());
    }

    private List<String> expectedErrorMessages() {
        return Arrays.asList(
                "offerUrl must not be null",
                "offerUrl must not be empty",
                "position must not be empty",
                "position must not be null",
                "salary must not be empty",
                "salary must not be null");
    }

    private String wrongOfferPostJson() {
        return "{\"companyName\":\"test\"}";
    }

}