package pl.emilfrankiewicz.offer.domain.exceptions;

import org.springframework.http.HttpStatus;

public interface SampleOfferErrorResponse {
    default OfferErrorResponse sampleOfferErrorResponse() {
        return new OfferErrorResponse("Not found offer with id: 666", HttpStatus.NOT_FOUND);
    }
}
