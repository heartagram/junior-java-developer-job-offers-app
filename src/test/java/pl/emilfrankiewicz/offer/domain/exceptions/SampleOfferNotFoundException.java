package pl.emilfrankiewicz.offer.domain.exceptions;

public interface SampleOfferNotFoundException {
    default OfferNotFoundException sampleOfferNotFoundException() {
        return new OfferNotFoundException("666");
    }
}
