package pl.emilfrankiewicz.offer.domain.exceptions;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class OfferErrorResponseHandlerTest implements SampleOfferErrorResponse, SampleOfferNotFoundException {

    @Test
    void should_return_correct_response() {
        //given
        OfferControllerErrorHandler offerControllerErrorHandler = new OfferControllerErrorHandler();
        final OfferNotFoundException givenException = sampleOfferNotFoundException();
        final OfferErrorResponse expectedResponse = sampleOfferErrorResponse();

        //when
        final OfferErrorResponse actualResponse = offerControllerErrorHandler.offerNotFound(givenException);

        //then
        assertThat(actualResponse).isEqualTo(expectedResponse);
    }
}
