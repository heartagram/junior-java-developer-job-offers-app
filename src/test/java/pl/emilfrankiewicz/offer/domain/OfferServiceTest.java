package pl.emilfrankiewicz.offer.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import pl.emilfrankiewicz.offer.domain.dto.OfferDto;
import pl.emilfrankiewicz.offer.domain.dto.SampleOffer;
import pl.emilfrankiewicz.offer.domain.dto.SampleOfferDto;
import pl.emilfrankiewicz.offer.domain.exceptions.OfferNotFoundException;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.catchThrowable;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class OfferServiceTest implements SampleOfferDto, SampleOffer {

    OfferRepository offerRepository = Mockito.mock(OfferRepository.class);

    @BeforeEach
    public void init() {
        when(offerRepository.findAll()).thenReturn(Arrays.asList(cybersourceOffer(), cdqPolandOffer()));
        when(offerRepository.findById("24ee32b6-6b15-11eb-9439-0242ac130002")).thenReturn(Optional.of(cdqPolandOffer()));
        when(offerRepository.save(cdqPolandOfferWithoutId())).thenReturn(cdqPolandOfferWithId());
    }

    @Test
    public void should_return_filled_list_of_offers() {
        //given
        OfferService offerService = new OfferService(offerRepository);
        List<OfferDto> expectedOffers = Arrays.asList(cybersourceOfferDto(), cdqPolandOfferDto());

        //when
        List<OfferDto> allOffers = offerService.findAllOffers();

        //then
        assertEquals(allOffers, expectedOffers);
    }

    @Test
    public void should_return_offer_of_given_id() {
        //given
        OfferService offerService = new OfferService(offerRepository);

        //when
        OfferDto expectedOffer = offerService.findOfferById("24ee32b6-6b15-11eb-9439-0242ac130002");

        //then
        assertThat(expectedOffer).isEqualTo(cdqPolandOfferDto());
    }

    @Test
    public void should_throw_offer_not_found_exception_when_no_offer_with_given_id() {
        //given
        OfferService offerService = new OfferService(offerRepository);

        //when
        Throwable thrown = catchThrowable(() -> offerService.findOfferById("666"));

        //then
        assertThat(thrown)
                .isInstanceOf(OfferNotFoundException.class)
                .hasMessage("Not found offer with id: 666");
    }

    @Test
    public void should_correctly_parse_dto_and_add_to_repository() {
        //given
        OfferService offerService = new OfferService(offerRepository);
        final OfferDto givenOfferDto = cdqPolandOfferDtoWithoutId();
        final OfferDto expectedOfferDto = cdqPolandOfferDtoWithId();

        //when
        final OfferDto saveResult = offerService.saveOffer(givenOfferDto);

        //then
        then(saveResult).isEqualTo(expectedOfferDto);
        verify(offerRepository, times(1)).save(any());
    }
}