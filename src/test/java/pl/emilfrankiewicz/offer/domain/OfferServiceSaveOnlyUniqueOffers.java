package pl.emilfrankiewicz.offer.domain;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import pl.emilfrankiewicz.infrastracture.offer.configuration.dto.JobOfferDto;
import pl.emilfrankiewicz.offer.domain.dto.SampleJobOfferDto;
import pl.emilfrankiewicz.offer.domain.dto.SampleOffer;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.List;

@SpringBootTest
@ActiveProfiles("container")
@Testcontainers
public class OfferServiceSaveOnlyUniqueOffers implements SampleJobOfferDto, SampleOffer {

    @Autowired
    private OfferService offerService;

    @Autowired
    private OfferRepository offerRepository;
    @Container
    private static final MongoDBContainer mongoDBContainer = new MongoDBContainer("mongo");

    static {
        mongoDBContainer.start();
        System.setProperty("DB_PORT", String.valueOf(mongoDBContainer.getFirstMappedPort()));
    }

    @Test
    public void should_save_offers_only_if_url_is_unique() {
        List<Offer> saved_One_Offer = Arrays.asList(cdqPolandOfferWithoutId());
        offerService.saveAll(saved_One_Offer);
        assertThat(offerRepository.findAll().size()).isEqualTo(1);

        List<JobOfferDto> saved_Two_Offers = Arrays.asList(cybersourceJobOfferDto(), cdqPolandJobOfferDto());
        offerService.saveAllJobOfferDto(saved_Two_Offers);
        assertThat(offerRepository.findAll().size()).isEqualTo(2);

        assertThat(offerRepository.existsByOfferUrl(cdqPolandOfferWithoutId().getOfferUrl())).isTrue();
    }
}