package pl.emilfrankiewicz.offer.domain.dto;

import pl.emilfrankiewicz.infrastracture.offer.configuration.dto.JobOfferDto;

public interface SampleJobOfferDto {

    default JobOfferDto cdqPolandJobOfferDto() {
        return new JobOfferDto(
                "Junior DevOps Engineer",
                "CDQ Poland",
                "8k - 14k PLN",
                "https://nofluffjobs.com/pl/job/junior-devops-engineer-cdq-poland-wroclaw-gnymtxqd"
        );
    }

    default JobOfferDto cybersourceJobOfferDto() {
        return new JobOfferDto(
                "Software Engineer - Mobile (m/f/d)",
                "Cybersource",
                "4k - 8k PLN",
                "https://nofluffjobs.com/pl/job/software-engineer-mobile-m-f-d-cybersource-poznan-entavdpn"
        );
    }
}
