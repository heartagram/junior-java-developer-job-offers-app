package pl.emilfrankiewicz.offer.domain.dto;

import pl.emilfrankiewicz.offer.domain.OfferMapper;

public interface SampleOfferDto extends SampleOffer {

    default OfferDto cdqPolandOfferDto() {
        return OfferMapper.mapToOfferDto(cdqPolandOffer());
    }

    default OfferDto cybersourceOfferDto() {
        return OfferMapper.mapToOfferDto(cybersourceOffer());
    }

    default OfferDto cdqPolandOfferDtoWithId() {
        return OfferMapper.mapToOfferDto(cdqPolandOfferWithId());
    }

    default OfferDto cybersourceOfferDtoWithId() {
        return OfferMapper.mapToOfferDto(cybersourceOfferWithId());
    }

    default OfferDto cdqPolandOfferDtoWithoutId() {
        return OfferMapper.mapToOfferDto(cdqPolandOfferWithoutId());
    }
}
