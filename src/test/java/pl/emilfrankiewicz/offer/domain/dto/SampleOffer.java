package pl.emilfrankiewicz.offer.domain.dto;

import pl.emilfrankiewicz.offer.domain.Offer;

public interface SampleOffer {

    default Offer cdqPolandOffer() {
        return new Offer("24ee32b6-6b15-11eb-9439-0242ac130002",
                "Junior DevOps Engineer",
                "8k - 14k PLN",
                "https://nofluffjobs.com/pl/job/junior-devops-engineer-cdq-poland-wroclaw-gnymtxqd",
                "CDQ Poland");
    }

    default Offer cybersourceOffer() {
        return new Offer("7b3e02b3-6b1a-4e75-bdad-cef5b279b074",
                "Software Engineer - Mobile (m/f/d)",
                "4k - 8k PLN",
                "https://nofluffjobs.com/pl/job/software-engineer-mobile-m-f-d-cybersource-poznan-entavdpn",
                "Cybersource");
    }

    default Offer cdqPolandOfferWithId() {
        return new Offer("640af8e8bb25b76f758d0e27",
                "CDQ Poland",
                "Junior DevOps Engineer",
                "8k - 14k PLN",
                "https://nofluffjobs.com/pl/job/junior-devops-engineer-cdq-poland-wroclaw-gnymtxqd");
    }

    default Offer cybersourceOfferWithId() {
        return new Offer("640af8e8bb25b76f758d0e26",
                "Cybersource",
                "Software Engineer - Mobile (m/f/d)",
                "4k - 8k PLN",
                "https://nofluffjobs.com/pl/job/software-engineer-mobile-m-f-d-cybersource-poznan-entavdpn");
    }

    default Offer cdqPolandOfferWithoutId() {
        final Offer offer = new Offer();
        offer.setCompanyName("CDQ Poland");
        offer.setPosition("Junior DevOps Engineer");
        offer.setSalary("8k - 14k PLN");
        offer.setOfferUrl("https://nofluffjobs.com/pl/job/junior-devops-engineer-cdq-poland-wroclaw-gnymtxqd");
        return offer;
    }

    default Offer cybersourceOfferWithoutId() {
        final Offer offer = new Offer();
        offer.setCompanyName("Cybersource");
        offer.setPosition("Software Engineer - Mobile (m/f/d)");
        offer.setSalary("4k - 8k PLN");
        offer.setOfferUrl("https://nofluffjobs.com/pl/job/software-engineer-mobile-m-f-d-cybersource-poznan-entavdpn");
        return offer;
    }
}
