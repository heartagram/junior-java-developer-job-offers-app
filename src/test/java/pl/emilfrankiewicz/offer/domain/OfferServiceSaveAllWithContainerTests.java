package pl.emilfrankiewicz.offer.domain;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import pl.emilfrankiewicz.offer.domain.dto.SampleOffer;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.BDDAssertions.then;
import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest
@ActiveProfiles("container")
@Testcontainers
public class OfferServiceSaveAllWithContainerTests implements SampleOffer {

    @Autowired
    private OfferService offerService;

    @Autowired
    private OfferRepository offerRepository;
    @Container
    private static final MongoDBContainer mongoDBContainer = new MongoDBContainer("mongo");

    static {
        mongoDBContainer.start();
        System.setProperty("DB_PORT", String.valueOf(mongoDBContainer.getFirstMappedPort()));
    }

    @Test
    public void should_save_all_offers() {

        List<Offer> offersToSave = Arrays.asList(cdqPolandOfferWithoutId(), cybersourceOfferWithoutId());
        then(offerRepository.findAll()).doesNotContainAnyElementsOf(offersToSave);

        List<Offer> savedOffers = offerService.saveAll(offersToSave);

        assertThat(savedOffers).containsAnyElementsOf(offersToSave);
    }
}
