package pl.emilfrankiewicz.offer.domain;

import org.assertj.core.api.AssertionsForClassTypes;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import pl.emilfrankiewicz.offer.domain.dto.OfferDto;
import pl.emilfrankiewicz.offer.domain.dto.SampleOffer;
import pl.emilfrankiewicz.offer.domain.dto.SampleOfferDto;
import pl.emilfrankiewicz.offer.domain.exceptions.OfferNotFoundException;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.catchThrowable;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("container")
@Testcontainers
public class OfferServiceWithContainerTests implements SampleOfferDto, SampleOffer {

    @Autowired
    private OfferService offerService;

    @Container
    private static final MongoDBContainer mongoDBContainer = new MongoDBContainer("mongo");

    static {
        mongoDBContainer.start();
        System.setProperty("DB_PORT", String.valueOf(mongoDBContainer.getFirstMappedPort()));
    }


    @Test
    public void should_return_all_offers() {
        //given
        List<OfferDto> expectedOffers = Arrays.asList(cybersourceOfferDtoWithId(), cdqPolandOfferDtoWithId());

        //when
        List<OfferDto> allOffers = offerService.findAllOffers();

        //then
        assertEquals(expectedOffers, allOffers);

    }

    @Test
    public void should_return_offer_with_given_id() {
        //given
        OfferDto expectedOffer = cybersourceOfferDtoWithId();

        //when
        OfferDto offerFromDB = offerService.findOfferById("640af8e8bb25b76f758d0e26");

        //then
        assertEquals(expectedOffer, offerFromDB);
    }

    @Test
    public void should_throw_offer_not_found_exception_when_no_offer_with_given_id() {
        //given
        String offerId = "666";

        //when
        Throwable thrown = catchThrowable(() -> offerService.findOfferById(offerId));

        //then
        AssertionsForClassTypes.assertThat(thrown)
                .isInstanceOf(OfferNotFoundException.class)
                .hasMessage("Not found offer with id: 666");
    }
}
