package pl.emilfrankiewicz.offer.domain;


import org.assertj.core.api.AssertionsForClassTypes;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import pl.emilfrankiewicz.offer.domain.dto.OfferDto;
import pl.emilfrankiewicz.offer.domain.dto.SampleOfferDto;
import pl.emilfrankiewicz.offer.domain.exceptions.DuplicateKeyException;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.catchThrowable;
import static org.assertj.core.api.BDDAssertions.then;


@SpringBootTest
@ActiveProfiles("container")
@Testcontainers
public class OfferServiceSaveWithContainerTests implements SampleOfferDto {

    @Autowired
    private OfferService offerService;

    @Autowired
    private OfferRepository offerRepository;
    @Container
    private static final MongoDBContainer mongoDBContainer = new MongoDBContainer("mongo");

    private final String URL = "https://nofluffjobs.com/pl/job/junior-devops-engineer-cdq-poland-wroclaw-gnymtxqd";

    static {
        mongoDBContainer.start();
        System.setProperty("DB_PORT", String.valueOf(mongoDBContainer.getFirstMappedPort()));
    }

    //Remember - flush database before start test.
    @Test
    void should_add_offer_using_service() {

        OfferDto offerDtoToSave = cdqPolandOfferDtoWithoutId();

        then(offerRepository.findByOfferUrl(URL)).isNotPresent();

        final OfferDto savedOffer = offerService.saveOffer(offerDtoToSave);

        final Optional<Offer> offer = offerRepository.findByOfferUrl(URL);
        assertThat(offer).isPresent();

        final Offer actualOffer = offer.get();
        assertThat(actualOffer.getOfferUrl()).isEqualTo(savedOffer.getOfferUrl());
        assertThat(actualOffer.getCompanyName()).isEqualTo(savedOffer.getCompanyName());
        assertThat(actualOffer.getPosition()).isEqualTo(savedOffer.getPosition());
        assertThat(actualOffer.getSalary()).isEqualTo(savedOffer.getSalary());
    }

    //Remember - flush database before start test.
    @Test
    void should_not_add_offer_when_offer_with_url_already_exists_in_database() {
        final OfferDto offerDtoToSave = cdqPolandOfferDtoWithoutId();
        final Offer sameUrlOffer = cdqPolandOfferWithoutId();
        offerRepository.save(sameUrlOffer);
        then(offerRepository.findByOfferUrl(URL)).isPresent();

        Throwable thrown = catchThrowable(() -> offerService.saveOffer(offerDtoToSave));

        AssertionsForClassTypes.assertThat(thrown)
                .isInstanceOf(DuplicateKeyException.class)
                .hasMessage("Already exist offer with URL: " + URL);
    }
}
