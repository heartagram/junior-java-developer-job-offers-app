package pl.emilfrankiewicz.offer.domain.configuration;

import org.springframework.context.annotation.Bean;
import pl.emilfrankiewicz.offer.OfferController;
import pl.emilfrankiewicz.offer.domain.OfferRepository;
import pl.emilfrankiewicz.offer.domain.OfferService;
import pl.emilfrankiewicz.offer.domain.dto.OfferDto;
import pl.emilfrankiewicz.offer.domain.dto.SampleOfferDto;
import pl.emilfrankiewicz.offer.domain.exceptions.OfferControllerErrorHandler;
import pl.emilfrankiewicz.offer.domain.exceptions.SampleOfferNotFoundException;

import java.util.Arrays;
import java.util.Optional;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MockMvcConfig implements SampleOfferDto, SampleOfferNotFoundException {

    @Bean
    OfferService offerService() {
        OfferRepository offerRepository = mock(OfferRepository.class);
        when(offerRepository.findAll()).thenReturn(Arrays.asList(cybersourceOffer(), cdqPolandOffer()));
        when(offerRepository.findById("24ee32b6-6b15-11eb-9439-0242ac130002")).thenReturn(Optional.of(cdqPolandOffer()));
        when(offerRepository.save(cdqPolandOffer())).thenReturn(cdqPolandOffer());
        return new OfferService(offerRepository) {
            @Override
            public OfferDto saveOffer(OfferDto offerDto) {
                return cdqPolandOfferDto();
            }
        };
    }

    @Bean
    OfferController offerController(OfferService offerService) {
        return new OfferController(offerService);
    }

    @Bean
    OfferControllerErrorHandler offerControllerErrorHandler() {
        return new OfferControllerErrorHandler();
    }

}
