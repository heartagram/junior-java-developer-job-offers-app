package pl.emilfrankiewicz.infrastracture.offer.configuration.dto;

import lombok.*;

@Getter
@Setter
@Builder
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class JobOfferDto {
    private String title;
    private String company;
    private String salary;
    private String offerUrl;
}
