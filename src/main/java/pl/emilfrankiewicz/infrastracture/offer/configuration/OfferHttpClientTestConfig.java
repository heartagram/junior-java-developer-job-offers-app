package pl.emilfrankiewicz.infrastracture.offer.configuration;

import org.springframework.web.client.RestTemplate;
import pl.emilfrankiewicz.infrastracture.offer.client.OfferClient;
import pl.emilfrankiewicz.infrastracture.offer.client.RemoteOfferClient;

public class OfferHttpClientTestConfig extends OfferHttpClientConfig {

    public RemoteOfferClient remoteOfferTestClient(String uri, int port) {
        final RestTemplate restTemplate = restTemplate();
        return remoteOfferClient(restTemplate, uri, port);
    }
}
