package pl.emilfrankiewicz.infrastracture.offer.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import pl.emilfrankiewicz.infrastracture.offer.client.OfferClient;
import pl.emilfrankiewicz.infrastracture.offer.client.RemoteOfferClient;

import java.time.Duration;

@Configuration
public class OfferHttpClientConfig {

    @Bean
    RestTemplate restTemplate() {

        return new RestTemplateBuilder()
                .setConnectTimeout(Duration.ofSeconds(500))
                .setReadTimeout(Duration.ofSeconds(500))
                .build();
    }

    @Bean
    RemoteOfferClient remoteOfferClient(RestTemplate restTemplate,
                                        @Value("${offer.http.client.config.uri:http://ec2-18-157-77-69.eu-central-1.compute.amazonaws.com}") String uri,
                                        @Value("${offer.http.client.config.port:5057}") int port) {
        return new OfferClient(restTemplate, uri, port);
    }

}
