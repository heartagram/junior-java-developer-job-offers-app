package pl.emilfrankiewicz.infrastracture.offer.client;

import lombok.AllArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;

import org.springframework.web.util.UriComponentsBuilder;
import pl.emilfrankiewicz.infrastracture.offer.configuration.dto.JobOfferDto;

@AllArgsConstructor
public class OfferClient implements RemoteOfferClient {

    private RestTemplate restTemplate;
    private final String uri;
    private final int port;

    public List<JobOfferDto> getOffers() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<HttpHeaders> requestEntity = new HttpEntity<>(headers);

        try {

            final String url = UriComponentsBuilder.fromHttpUrl(getUrlForService("/offers")).toUriString();
            ResponseEntity<List<JobOfferDto>> response = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
                    new ParameterizedTypeReference<List<JobOfferDto>>() {
                    });
            final List<JobOfferDto> jobOfferDTO = response.getBody();

            return (jobOfferDTO != null) ? jobOfferDTO : Collections.emptyList();
        } catch (ResourceAccessException e) {
            return Collections.emptyList();
        }
    }

    private String getUrlForService(String service) {
        return uri + ":" + port + service;
    }

}
