package pl.emilfrankiewicz.infrastracture.offer.client;

import pl.emilfrankiewicz.infrastracture.offer.configuration.dto.JobOfferDto;
import java.util.List;

public interface RemoteOfferClient {
    List<JobOfferDto> getOffers();
}
