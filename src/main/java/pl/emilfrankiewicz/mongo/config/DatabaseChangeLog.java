package pl.emilfrankiewicz.mongo.config;

import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;
import pl.emilfrankiewicz.offer.domain.Offer;
import pl.emilfrankiewicz.offer.domain.OfferRepository;

import java.util.Arrays;

@ChangeLog(order = "001")
public class DatabaseChangeLog {

    @ChangeSet(order = "001", id = "seedDatabase", author = "emil")
    public void seedDataBase(OfferRepository offerRepository) {
        offerRepository.insert(Arrays.asList(cyberSourceOffer(), cdqPolandOffer()));
    }

    private Offer cyberSourceOffer() {
        final Offer cybersource = new Offer();
        cybersource.setOfferUrl("https://nofluffjobs.com/pl/job/software-engineer-mobile-m-f-d-cybersource-poznan-entavdpn");
        cybersource.setPosition("Software Engineer - Mobile (m/f/d)");
        cybersource.setSalary("4k - 8k PLN");
        cybersource.setCompanyName("Cybersource");
        return cybersource;
    }

    private Offer cdqPolandOffer() {
        final Offer cybersource = new Offer();
        cybersource.setOfferUrl("https://nofluffjobs.com/pl/job/junior-devops-engineer-cdq-poland-wroclaw-gnymtxqd");
        cybersource.setPosition("Junior DevOps Engineer");
        cybersource.setSalary("8k - 14k PLN");
        cybersource.setCompanyName("CDQ Poland");
        return cybersource;
    }

}


