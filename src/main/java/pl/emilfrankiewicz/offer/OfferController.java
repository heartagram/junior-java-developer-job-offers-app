package pl.emilfrankiewicz.offer;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.emilfrankiewicz.offer.domain.OfferService;
import pl.emilfrankiewicz.offer.domain.dto.OfferDto;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
@Slf4j
@RequestMapping(value = "/offers")
public class OfferController {

    OfferService offerService;

    @GetMapping
    public ResponseEntity<List<OfferDto>> findAllOffers() {
        return ResponseEntity.ok(offerService.findAllOffers());
    }

    @GetMapping("/{id}")
    public ResponseEntity<OfferDto> findOfferById(@PathVariable String id) {
        log.info(String.valueOf(id));
        return ResponseEntity.ok(offerService.findOfferById(id));
    }

    @PostMapping
    public ResponseEntity<OfferDto> addOffer(@Valid @RequestBody OfferDto offerDto) {
        return ResponseEntity.ok(offerService.saveOffer(offerDto));
    }

}
