package pl.emilfrankiewicz.offer.domain.scheduling;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pl.emilfrankiewicz.infrastracture.offer.client.RemoteOfferClient;
import pl.emilfrankiewicz.infrastracture.offer.configuration.dto.JobOfferDto;
import pl.emilfrankiewicz.offer.domain.Offer;
import pl.emilfrankiewicz.offer.domain.OfferService;

import java.util.List;

@Component
@RequiredArgsConstructor
public class HttpOffersScheduler {

    final private RemoteOfferClient remoteOfferClient;
    final private OfferService offerService;

    private static final Logger log = LoggerFactory.getLogger(HttpOffersScheduler.class);

    @Scheduled(fixedDelayString = "PT3H")
    public void getOffersFromHttpService() {
        final List<JobOfferDto> offerList = remoteOfferClient.getOffers();
        final List<Offer> addedOffer = offerService.saveAllJobOfferDto(offerList);
        log.info("Added new offer: ", addedOffer.size());
    }
}
