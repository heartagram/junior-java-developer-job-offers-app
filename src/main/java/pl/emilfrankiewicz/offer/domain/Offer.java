package pl.emilfrankiewicz.offer.domain;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Document
@Builder
public class Offer {

    @Id
    private String id;
    private String companyName;
    private String position;
    private String salary;

    @Indexed(unique = true)
    private String offerUrl;
}
