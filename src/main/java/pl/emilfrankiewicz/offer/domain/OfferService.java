package pl.emilfrankiewicz.offer.domain;

import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import pl.emilfrankiewicz.infrastracture.offer.configuration.dto.JobOfferDto;
import pl.emilfrankiewicz.offer.domain.dto.OfferDto;
import pl.emilfrankiewicz.offer.domain.exceptions.DuplicateKeyException;
import pl.emilfrankiewicz.offer.domain.exceptions.OfferNotFoundException;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class OfferService {

    private final OfferRepository offerRepository;

    @Cacheable(cacheNames = "/offers")
    public List<OfferDto> findAllOffers() {
        return offerRepository.findAll()
                .stream()
                .map(OfferMapper::mapToOfferDto)
                .collect(Collectors.toList());
    }

    public OfferDto findOfferById(String id) {
        return offerRepository.findById(id)
                .map(OfferMapper::mapToOfferDto)
                .orElseThrow(() -> new OfferNotFoundException(id));
    }

    public List<Offer> saveAll(List<Offer> offers) {
        return offerRepository.saveAll(offers);
    }

    public List<Offer> saveAllJobOfferDto(List<JobOfferDto> jobOfferDto) {
        return offerRepository.saveAll(mapOffersThatDoesntExistsInDatabase(jobOfferDto));
    }

    public List<Offer> mapOffersThatDoesntExistsInDatabase(List<JobOfferDto> jobOfferDto) {
        return jobOfferDto.stream()
                .filter(jobOffer -> !jobOffer.getOfferUrl().isEmpty())
                .filter(jobOffer -> !offerRepository.existsByOfferUrl(jobOffer.getOfferUrl()))
                .map(OfferMapper::mapToOfferFromJobOfferDto)
                .collect(Collectors.toList());
    }


    public OfferDto saveOffer(OfferDto offerDto) {
        final Offer offer = OfferMapper.mapToOfferFromOfferDto(offerDto);
        try {
            final Offer savedOffer = offerRepository.save(offer);
            return OfferMapper.mapToOfferDto(savedOffer);
        } catch (com.mongodb.DuplicateKeyException e) {
            throw new DuplicateKeyException(offerDto.getOfferUrl());
        }
    }

}
