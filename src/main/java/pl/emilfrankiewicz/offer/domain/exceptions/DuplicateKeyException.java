package pl.emilfrankiewicz.offer.domain.exceptions;

import lombok.Getter;

@Getter
public class DuplicateKeyException extends RuntimeException {

    private final String offerUrl;

    public DuplicateKeyException(String offerUrl) {
        super("Already exist offer with URL: " + offerUrl);
        this.offerUrl = offerUrl;
    }
}
