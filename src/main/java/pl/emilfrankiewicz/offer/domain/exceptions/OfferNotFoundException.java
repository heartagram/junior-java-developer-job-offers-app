package pl.emilfrankiewicz.offer.domain.exceptions;

import lombok.Getter;

@Getter
public class OfferNotFoundException extends RuntimeException {

    private String offerId;

    public OfferNotFoundException(String id) {
        super("Not found offer with id: " + id);
        this.offerId = id;
    }
}
