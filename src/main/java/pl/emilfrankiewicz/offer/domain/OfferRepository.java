package pl.emilfrankiewicz.offer.domain;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface OfferRepository extends MongoRepository<Offer, String> {

    boolean existsByOfferUrl(String offerUrl);
    Optional<Offer> findByOfferUrl(String offerUrl);
}
