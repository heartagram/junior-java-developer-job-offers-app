package pl.emilfrankiewicz.offer.domain;

import pl.emilfrankiewicz.infrastracture.offer.configuration.dto.JobOfferDto;
import pl.emilfrankiewicz.offer.domain.dto.OfferDto;

public class OfferMapper {
    public static OfferDto mapToOfferDto(Offer offer) {
        return OfferDto.builder()
                .id(offer.getId())
                .companyName(offer.getCompanyName())
                .position(offer.getPosition())
                .salary(offer.getSalary())
                .offerUrl(offer.getOfferUrl())
                .build();
    }

    public static Offer mapToOfferFromJobOfferDto(JobOfferDto jobOfferDto) {
        return Offer.builder()
                .companyName(jobOfferDto.getCompany())
                .position(jobOfferDto.getTitle())
                .salary(jobOfferDto.getSalary())
                .offerUrl(jobOfferDto.getOfferUrl())
                .build();
    }

    public static Offer mapToOfferFromOfferDto(OfferDto offerDto) {
        return Offer.builder()
                .companyName(offerDto.getCompanyName())
                .position(offerDto.getPosition())
                .salary(offerDto.getSalary())
                .offerUrl(offerDto.getOfferUrl())
                .build();
    }
}
